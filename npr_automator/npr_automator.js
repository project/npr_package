Drupal.behaviors.npr_automator = function(content){
 $('.npr_automator_toggle_status').click(function(){
  var status;
  var newText;
  if($(this).text() == 'Disabled'){
    status = 0;
    newText = 'Enabled';
  } else {
    newText = 'Disabled';
    status = 1;
  }
  $(this).text('Loading...');

  var naid = $(this).attr('id');
  $toggle = $(this);
  $.ajax({
    url: Drupal.settings.basePath + '?q=npr_automator/toggle_status/callback/' + status +'/'+naid,
    success: function(response){
      var result = Drupal.parseJson(response);
      if(result.success == 'yes'){
          $toggle.text(newText);
        } else {
          alert(Drupal.t('An internal error occcured.'));
        }
      }
    });
  });
}
